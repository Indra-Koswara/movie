package com.example.Movie.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.models.MovieCast;
import com.example.Movie.models.MovieCastId;
import com.example.Movie.modelsDTO.MovieCastDTO;
import com.example.Movie.repositories.MovieCastRepository;

@RestController
@RequestMapping("/api")
public class MovieCastController {

	@Autowired
	MovieCastRepository movieCastRepository;
	
	@PostMapping("/movie_cast/create")
	public HashMap<String, Object> createDataMovieCast(@Valid @RequestBody MovieCastDTO movieCastDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<MovieCast> listMovieCast = (ArrayList<MovieCast>) movieCastRepository.findAll();
		ModelMapper modelMapper = new ModelMapper();
		boolean isFound = false ;
		MovieCast mC = modelMapper.map(movieCastDTO, MovieCast.class);
		for (MovieCast movieCast : listMovieCast) {
			if (movieCast.getId().getActId() != movieCastDTO.getId().getActId() && movieCast.getId().getMovId() != movieCastDTO.getId().getMovId()) {
				movieCastRepository.save(mC);
				movieCastDTO = modelMapper.map(mC, MovieCastDTO.class);
				isFound = true;
			}
			else {
				isFound = false ;
			}
		}
		if (isFound == true) {
			result.put("Status", 200);
			result.put("Message", "Create New Movie Cast Success");
			result.put("Data", movieCastDTO);
		}
		else {
			result.put("Message", "Data ID yang kamu masukan sudah tersedia");
			result.put("Data", movieCastDTO);
		}
		
		
		
		return result;
	}
	
	@GetMapping("/movie_cast/readAll")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<MovieCast> listmCEntity =(ArrayList<MovieCast>) movieCastRepository.findAll();
		ArrayList<MovieCastDTO> listmCDTO = new ArrayList<MovieCastDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(MovieCast mC : listmCEntity) {
			MovieCastDTO mCDTO = modelMapper.map(mC, MovieCastDTO.class);
			listmCDTO.add(mCDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Show Data  Movie Cast Is Success");
		result.put("Data", listmCDTO);
		return result;
	}
	
	@PutMapping("/movie_cast/update/{actId}/{movId}")
	public HashMap<String, Object> updateData (@PathVariable(value = "actId") Long actId, @PathVariable (value = "movId")Long movId, 
			@Valid @RequestBody MovieCastDTO movieCastDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<MovieCast> listMCEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		boolean isFound = false ;
		
		ModelMapper modelMapper = new ModelMapper();
	
		
		for (MovieCast movieCast : listMCEntity) {
		     if(movieCast.getId().getActId() == actId && movieCast.getId().getMovId() == movId) {
		    	 movieCast.setRole(movieCastDTO.getRole());

	                movieCastRepository.save(movieCast);

	                movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
	                isFound = true ;
	            }
		     else {
		    	 isFound = false;
		     }
	        }
			if(isFound == true) {
		        result.put("Status", 200);
		        result.put("Message", "Update Movie Cast Success");
		        result.put("Data", movieCastDTO);
			}
			else {
				 result.put("Message", "Update Movie Cast Success");
			}
	        return result;
	    
		}
		
	@DeleteMapping("/movie_cast/delete/{actId}/{movId}")
	public HashMap<String, Object> deleteData (@PathVariable(value = "actId") Long actId, @PathVariable (value = "movId")Long movId){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		 MovieCastId movieCastIdEntity = new MovieCastId();
	        movieCastIdEntity.setActId(actId);
	        movieCastIdEntity.setMovId(movId);
		MovieCastDTO mCDTO = new MovieCastDTO();
		
		ArrayList<MovieCast> listMCEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		
		for (MovieCast movieCast : listMCEntity) {
			mCDTO = modelMapper.map(movieCast, MovieCastDTO.class);
			if(movieCast.getId().equals(movieCastIdEntity)) {
				movieCastRepository.delete(movieCast);
			}
		}
		result.put("Status", 200);
        result.put("Message", "Delete Movie Cast Success");
        result.put("Data", mCDTO);
        return result;
	}

}
