package com.example.Movie.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exception.ResourceNotFoundException;
import com.example.Movie.models.Reviewer;
import com.example.Movie.modelsDTO.ReviewerDTO;
import com.example.Movie.repositories.ReviewerRepository;

@RestController
@RequestMapping("/api")
public class ReviewerController {

	@Autowired
	ReviewerRepository reviewerRepository;
	
	@PostMapping("/reviewer/create")
	public HashMap<String, Object> createDataReviewer(@Valid @RequestBody ReviewerDTO reviewerDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Reviewer reviewer = modelMapper.map(reviewerDTO, Reviewer.class);
		reviewerRepository.save(reviewer);
		reviewerDTO = modelMapper.map(reviewer, ReviewerDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Reviwer Success");
		result.put("Data", reviewerDTO);
		
		return result;
	}
	@PutMapping("/reviewer/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Long id , @Valid @RequestBody ReviewerDTO reviewerDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Reviewer reviewer= reviewerRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Reviewer", "id", id));
		reviewer = modelMapper.map(reviewerDTO, Reviewer.class);
		reviewer.setId(id);
		
		reviewerRepository.save(reviewer);
		reviewerDTO = modelMapper.map(reviewer, ReviewerDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Data Reviwer With ID:" + id + " Is Success");
		result.put("Data", reviewerDTO);
		return result;
		
	}
	@DeleteMapping("/reviewer/delete/{id}")
	public HashMap<String, Object> deleteDataActor (@PathVariable(value = "id")  Long id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		ReviewerDTO reviewerDTO = new ReviewerDTO();
		Reviewer reviewer = reviewerRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Reviewer", "id", id));
		reviewerDTO = modelMapper.map(reviewer, ReviewerDTO.class);
		
		reviewerRepository.delete(reviewer);
		
		result.put("Status", 200);
		result.put("Message", "Delete Data Reviwer With ID:" + id + " Is Success");
		result.put("Data", reviewerDTO);
		return result;
		
	}
	@GetMapping("/reviewer/readAll")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Reviewer> listReviewerEntity = (ArrayList<Reviewer>) reviewerRepository.findAll();
		ArrayList<ReviewerDTO> listReviewerDTO =new ArrayList<ReviewerDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Reviewer reviewer : listReviewerEntity) {
			ReviewerDTO reviewerDTO = modelMapper.map(reviewer, ReviewerDTO.class);
			
			listReviewerDTO.add(reviewerDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Show Data Reviwer Is Success");
		result.put("Data", listReviewerDTO);
		return result;
	}
}
