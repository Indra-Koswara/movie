package com.example.Movie.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.models.MovieDirection;
import com.example.Movie.modelsDTO.MovieDirectionDTO;
import com.example.Movie.repositories.MovieDirectionRepository;

@RestController
@RequestMapping("/api")
public class MovieDirectionController {

	@Autowired
	MovieDirectionRepository movieDirectionRepository;
	
	@GetMapping("/movie_direction/readAll")
	public HashMap<String, Object> getAllData (){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<MovieDirection> listMDEntity = (ArrayList<MovieDirection>) movieDirectionRepository.findAll();
		ArrayList<MovieDirectionDTO> listMDDTO = new ArrayList<MovieDirectionDTO>();
		ModelMapper modelMapper = new ModelMapper();
		
			for(MovieDirection mD : listMDEntity) {
				MovieDirectionDTO mDDTO = modelMapper.map(mD, MovieDirectionDTO.class);
				listMDDTO.add(mDDTO);
			}
			result.put("Status", 200);
			result.put("Message", "Read data movie direction success");
			result.put("Data", listMDDTO);
		return result;
	
	}
	
	@PostMapping("/movie_direction/create")
	public HashMap<String, Object> createData (@Valid @RequestBody MovieDirectionDTO movieDirectionDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		MovieDirection mD = modelMapper.map(movieDirectionDTO, MovieDirection.class);
		movieDirectionRepository.save(mD);
		movieDirectionDTO = modelMapper.map(mD, MovieDirectionDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Read data movie direction success");
		result.put("Data", movieDirectionDTO);
	return result;
	}
}
