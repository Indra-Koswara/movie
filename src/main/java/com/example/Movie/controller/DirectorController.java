package com.example.Movie.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exception.ResourceNotFoundException;
import com.example.Movie.models.Director;
import com.example.Movie.modelsDTO.DirectorDTO;
import com.example.Movie.repositories.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {

	@Autowired
	DirectorRepository directorRepository;
	
	
	@PostMapping("/director/create")
	public HashMap<String, Object> createDataDirector (@Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Director director = modelMapper.map(directorDTO, Director.class);
		directorRepository.save(director);
		directorDTO = modelMapper.map(director, DirectorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New DirectorSuccess");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	@PutMapping("/director/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Long id , @Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Director director = directorRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Director", "id", id));
		director = modelMapper.map(directorDTO, Director.class);
		director.setId(id);
		
		directorRepository.save(director);
		directorDTO = modelMapper.map(director, DirectorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Data Director With ID:" + id + " Is Success");
		result.put("Data", directorDTO);
		return result;
		
	}
	@DeleteMapping("/director/delete/{id}")
	public HashMap<String, Object> deleteDataActor (@PathVariable(value = "id")  Long id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		DirectorDTO directorDTO = new DirectorDTO();
		Director directorEntity = directorRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Director", "id", id));
		directorDTO = modelMapper.map(directorEntity, DirectorDTO.class);
		
		directorRepository.delete(directorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Data Director With ID:" + id + " Is Success");
		result.put("Data", directorDTO);
		return result;
		
	}
	@GetMapping("/director/readAll")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Director> listDirectorEntity = (ArrayList<Director>) directorRepository.findAll();
		ArrayList<DirectorDTO> listDirectorDTO =new ArrayList<DirectorDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Director director : listDirectorEntity) {
			DirectorDTO directorDTO = modelMapper.map(director, DirectorDTO.class);
			
			listDirectorDTO.add(directorDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Show Data Director Is Success");
		result.put("Data", listDirectorDTO);
		return result;
	}
}
