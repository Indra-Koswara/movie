package com.example.Movie.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exception.ResourceNotFoundException;
import com.example.Movie.models.Movie;
import com.example.Movie.modelsDTO.MovieDTO;
import com.example.Movie.repositories.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieController {
	@Autowired
	MovieRepository movieRepository;
	
	@PostMapping("/movie/create")
	public HashMap<String, Object> createDataGenre(@Valid @RequestBody MovieDTO movieDTO ){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Movie movie = modelMapper.map(movieDTO, Movie.class);
		movieRepository.save(movie);
		movieDTO = modelMapper.map(movie, MovieDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	@PutMapping("/movie/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Long id , @Valid @RequestBody MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Movie movie= movieRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Movie", "id", id));
		movie = modelMapper.map(movieDTO, Movie.class);
		movie.setId(id);
		
		movieRepository.save(movie);
		movieDTO = modelMapper.map(movie, MovieDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Data Movie With ID:" + id + " Is Success");
		result.put("Data", movieDTO);
		return result;
		
	}
	
	@DeleteMapping("/movie/delete/{id}")
	public HashMap<String, Object> deleteDataActor (@PathVariable(value = "id")  Long id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		MovieDTO movieDTO = new MovieDTO();
		Movie movie = movieRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Movie", "id", id));
		movieDTO = modelMapper.map(movie, MovieDTO.class);
		
		movieRepository.delete(movie);
		
		result.put("Status", 200);
		result.put("Message", "Delete Data Movie With ID:" + id + " Is Success");
		result.put("Data", movieDTO);
		return result;
		
	}
	@GetMapping("/movie/readAll")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Movie> listMovieEntity = (ArrayList<Movie>) movieRepository.findAll();
		ArrayList<MovieDTO> listMovieDTO =new ArrayList<MovieDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Movie movie : listMovieEntity) {
			MovieDTO movieDTO = modelMapper.map(movie, MovieDTO.class);
			
			listMovieDTO.add(movieDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Show Data Movie Is Success");
		result.put("Data", listMovieDTO);
		return result;
	}
}
