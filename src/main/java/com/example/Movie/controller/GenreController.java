package com.example.Movie.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exception.ResourceNotFoundException;
import com.example.Movie.models.Genre;
import com.example.Movie.modelsDTO.GenreDTO;
import com.example.Movie.repositories.GenreRepository;

@RestController
@RequestMapping("/api")
public class GenreController {

	@Autowired
	GenreRepository genreRepository;
	
	@PostMapping("/genre/create")
	public HashMap<String, Object> createDataGenre(@Valid @RequestBody GenreDTO genreDTO ){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Genre genre = modelMapper.map(genreDTO, Genre.class);
		genreRepository.save(genre);
		genreDTO = modelMapper.map(genre, GenreDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	@PutMapping("/genre/update/{id}")
	public HashMap<String, Object> updateData (@PathVariable(value = "id") Long id , @Valid @RequestBody GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Genre genre= genreRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Genre", "id", id));
		genre = modelMapper.map(genreDTO, Genre.class);
		genre.setId(id);
		
		genreRepository.save(genre);
		genreDTO = modelMapper.map(genre, GenreDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Data Genre With ID:" + id + " Is Success");
		result.put("Data", genreDTO);
		return result;
		
	}
	@DeleteMapping("/genre/delete/{id}")
	public HashMap<String, Object> deleteDataActor (@PathVariable(value = "id")  Long id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		GenreDTO genreDTO = new GenreDTO();
		Genre genre = genreRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Genre", "id", id));
		genreDTO = modelMapper.map(genre, GenreDTO.class);
		
		genreRepository.delete(genre);
		
		result.put("Status", 200);
		result.put("Message", "Delete Data Genre With ID:" + id + " Is Success");
		result.put("Data", genreDTO);
		return result;
		
	}
	@GetMapping("/genre/readAll")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Genre> listGenreEntity = (ArrayList<Genre>) genreRepository.findAll();
		ArrayList<GenreDTO> listGenreDTO =new ArrayList<GenreDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Genre genre : listGenreEntity) {
			GenreDTO genreDTO = modelMapper.map(genre, GenreDTO.class);
			
			listGenreDTO.add(genreDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Show Data Genre Is Success");
		result.put("Data", listGenreDTO);
		return result;
	}
}
