package com.example.Movie.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exception.ResourceNotFoundException;
import com.example.Movie.models.Actor;
import com.example.Movie.modelsDTO.ActorDTO;
import com.example.Movie.repositories.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorController {

	@Autowired
	ActorRepository actorRepository;
	
	//untuk membuat data baru
	@PostMapping("/actor/create")
	public HashMap<String, Object> createDataActor(@Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Actor actorEntity = modelMapper.map(actorDTO, Actor.class);
		
		actorRepository.save(actorEntity);
		actorDTO = modelMapper.map(actorEntity, ActorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
	//untuk update data
	@PutMapping("/actor/update/{id}")
	public HashMap<String, Object> updateDataActor (@PathVariable(value = "id") Long id, @Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		Actor actor = actorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Actor", "id", id));
		actor = modelMapper.map(actorDTO, Actor.class);
		actor.setId(id);
		
		actorRepository.save(actor);
		actorDTO = modelMapper.map(actor, ActorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Data Actor With ID:" + id + " Is Success");
		result.put("Data", actorDTO);
		return result;
	}
	
	//delete data
	@DeleteMapping("/actor/delete/{id}")
	public HashMap<String, Object> deleteDataActor (@PathVariable(value = "id")  Long id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ModelMapper modelMapper = new ModelMapper();
		ActorDTO actorDTO = new ActorDTO();
		Actor actorEntity = actorRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Actor", "id", id));
		actorDTO = modelMapper.map(actorEntity, ActorDTO.class);
		
		actorRepository.delete(actorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Data Actor With ID:" + id + " Is Success");
		result.put("Data", actorDTO);
		return result;
		
	}
	//untuk menampilkan seluruh data
	@GetMapping("/actor/readAll")
	public HashMap<String, Object> getAllData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Actor> listActorEntity = (ArrayList<Actor>) actorRepository.findAll();
		ArrayList<ActorDTO> listActorDTO =new ArrayList<ActorDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (Actor actor : listActorEntity) {
			ActorDTO actorDTO = modelMapper.map(actor, ActorDTO.class);
			
			listActorDTO.add(actorDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Show Data Actor Is Success");
		result.put("Data", listActorDTO);
		return result;
	}
}
