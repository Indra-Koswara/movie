package com.example.Movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.Movie.models.Actor;
@Repository
public interface ActorRepository extends JpaRepository<Actor, Long>{

}
