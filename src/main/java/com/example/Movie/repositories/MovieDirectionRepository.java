package com.example.Movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.Movie.models.MovieDirection;
@Repository
public interface MovieDirectionRepository extends JpaRepository<MovieDirection, Object> {

}
