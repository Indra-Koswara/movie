package com.example.Movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.Movie.models.Reviewer;
@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{

}
