package com.example.Movie.modelsDTO;


import com.example.Movie.models.MovieCastId;

public class MovieCastDTO {
	private MovieCastId id;
	private ActorDTO actor;
	private MovieDTO movie;
	private String role;
	
	public MovieCastDTO() {
		
	}

	public MovieCastDTO(MovieCastId id, ActorDTO actor, MovieDTO movie, String role) {
		
		this.id = id;
		this.actor = actor;
		this.movie = movie;
		this.role = role;
	}

	public MovieCastId getId() {
		return id;
	}

	public void setId(MovieCastId id) {
		this.id = id;
	}

	public ActorDTO getActor() {
		return actor;
	}

	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	public MovieDTO getMovie() {
		return movie;
	}

	public void setMovie(MovieDTO movie) {
		this.movie = movie;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
