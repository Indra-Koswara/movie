package com.example.Movie.modelsDTO;


import com.example.Movie.models.MovieDirectionId;

public class MovieDirectionDTO {

	private MovieDirectionId id;
	private ActorDTO actor;
	private DirectorDTO director;
	private MovieDTO movie;
	private String role;
	
	public MovieDirectionDTO() {
		
	}

	public MovieDirectionDTO(MovieDirectionId id, ActorDTO actor, DirectorDTO director, MovieDTO movie, String role) {
		
		this.id = id;
		this.actor = actor;
		this.director = director;
		this.movie = movie;
		this.role = role;
	}

	public MovieDirectionId getId() {
		return id;
	}

	public void setId(MovieDirectionId id) {
		this.id = id;
	}

	public ActorDTO getActor() {
		return actor;
	}

	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	public DirectorDTO getDirector() {
		return director;
	}

	public void setDirector(DirectorDTO director) {
		this.director = director;
	}

	public MovieDTO getMovie() {
		return movie;
	}

	public void setMovie(MovieDTO movie) {
		this.movie = movie;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
